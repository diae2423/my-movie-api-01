from fastapi import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()


        
@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200) #/movies es la direccion donde estará
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result)) #lo convierte a JSON

@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie)
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie: # significa que puede ser un id de 1 al 2000
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "no encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie]) #query pone prametros o requierimientos extra para valaidar una entrada
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)): #se pone el tipo de dato que usa el parametro,
    db = Session()
    result = MovieService(db).get_movies_by_category(category)
    #el category es por la cualidad del diccionario
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.post('/movies', tags=['movies'], response_model=dict)
def create_movie(movie : Movie) -> dict:
    db = Session()
    #usamos el modelo y le pasamos la info que vamos a registrar
    MovieService(db).create_movie(movie)
    return JSONResponse(content={"message": "Se ha registrado la pelicula"})


@movie_router.delete('/movies/{id}', tags=['movies'])
def delete_movie(id: int):
    db = Session()
    MovieService(db).delete_movie(id)


    return JSONResponse(content={"message": "Se ha Eliminado la pelicula"})



@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def updateMovies(id: int, movie: Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=200, content={"message": "NO ENCONTRADO"})
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={"message": "ACTUALIZADO"})