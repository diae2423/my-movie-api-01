from fastapi import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session, engine, Base
from models.categoria import Categoria as CategoriaModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.categoria import CategoriaService
from services.libro import LibroService
from schemas.categoria import Categoria
from models.libro import Libro as LibroModel

categoria_router = APIRouter()



@categoria_router.get('/categoria', tags=['categoria'], response_model=List[Categoria], status_code=200)
def get_categorias() -> List[Categoria]:
    db = Session()
    result = CategoriaService(db).get_categotias()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@categoria_router.get('/categoria/{codigo}', tags=['categoria'], response_model=Categoria)
def get_categoria(codigo: int) -> Categoria:
    db = Session()
    result = CategoriaService(db).get_categoria(codigo)
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@categoria_router.delete('/categoria/{codigo}', tags=['categoria'])
def delete_categoria(codigo: int):
    db = Session()

    resultDelete = db.query(LibroModel).filter(LibroModel.codigo==codigo).first()
    if resultDelete:
        return JSONResponse(status_code=400, content={"message": "No puedes eliminar una categoria con libros asignados"})
    CategoriaService(db).delete_categoria(codigo)
    return JSONResponse(status_code=200, content={'message': "Se ha Eliminado la categoria"})

@categoria_router.post('/categoria', tags=['categoria'])
def create_categoria(categorias : Categoria) -> dict:
    db = Session()
    CategoriaService(db).create_categoria(categorias)
    return JSONResponse(status_code=200, content={"message": "Nueva categoria registrada"})

@categoria_router.put('/categoria/{codigo}', tags=['categoria'], response_model=dict, status_code=200)
def updateCategorias(codigo: int, categorias: Categoria) -> dict:
    db = Session()
    result = CategoriaService(db).get_categoria(codigo)

    if not result:
        return JSONResponse(status_code=404, content={"message": "no encontrado"})
    
    CategoriaService(db).update_categoria(codigo, categorias)

    
    return JSONResponse(status_code=202, content={"message": "Se ha modificado la categoria"})


#----------------------------------------------------------------------------------------

