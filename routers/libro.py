from fastapi import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session, engine, Base
from models.libro import Libro as LibroModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.libro import LibroService
from services.categoria import CategoriaService
from schemas.libro import Libro
from sqlalchemy import func
from models.categoria import Categoria as CategoriaModel

libro_router = APIRouter()



def get_next_codigo():
    db = Session()
    max_codigo = db.query(func.max(LibroModel.codigo)).scalar()
    db.close()
    return (max_codigo or 0) + 1

@libro_router.get('/libros', tags=['libros'], response_model=List[Libro], status_code=200)
def get_libros() -> List[Libro]:
    db = Session()
    result = LibroService(db).get_libros()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@libro_router.get('/libros/{codigo}', tags=['libros'], response_model=Libro)
def get_libro(codigo: int = Path(ge=1, le=2000)) -> Libro:
    db = Session()
    result = LibroService(db).get_libro(codigo)
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@libro_router.get('/libros/', tags=['libros'], response_model=List[Libro])
def get_libros_by_categoria(categoria: str = Query(min_length=5, max_length=15)):
    db = Session()
    result = LibroService(db).get_libros_by_category(categoria)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@libro_router.delete('/libros/{codigo}', tags=['libros'])
def delete_libro(codigo: int):
    db = Session()
    result = db.query(LibroModel).filter(LibroModel.codigo == codigo).first()
    
    LibroService(db).delete_libro(codigo)

    return JSONResponse(status_code=200, content={"message": "Se ha Eliminado el Libro"})

@libro_router.post('/libros', tags=['libros'], response_model=dict)
def create_libro(libros : Libro) -> dict:
    db = Session()

    resultCat = db.query(CategoriaModel).filter(CategoriaModel.nombre == libros.categoria).first()

    if not resultCat:
        return JSONResponse(status_code=400, content={'message': "No se pueden agregar libros sin categoria existente"})
    libros.codigo = get_next_codigo()
    LibroService(db).create_libro(libros)

    return JSONResponse(status_code=200, content={'message': "Se agregó el libro"})
    

@libro_router.put('/libros/{codigo}', tags=['libros'], response_model=dict, status_code=200)
def updateLibros(codigo: int, libros: Libro) -> dict:
    db = Session()
    result = LibroService(db).get_libro(codigo)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Libro no encontrado"})
    
    resultCatAc = db.query(CategoriaModel).filter(CategoriaModel.nombre==libros.categoria).first()
    if not resultCatAc:
        return JSONResponse(status_code=400, content={"message": "No se puede modificar libros con categorías no existentes"})
    
    LibroService(db).update_libro(codigo, libros)
    return JSONResponse(status_code=202,content={"message": "Se ha modificado el libro"})

    


#----------------------------------------------------------------------------------------

