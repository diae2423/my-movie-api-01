from fastapi import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session, engine, Base
from models.computadora import Computadora as ComputadoraModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.computadora import ComputadoraService
from schemas.compu import Computadora

compu_router = APIRouter()


        
@compu_router.get('/compus', tags=['compus'], response_model=List[Computadora], status_code=200) #/Computadoras es la direccion donde estará
def get_compus() -> List[Computadora]:
    db = Session()
    result = ComputadoraService(db).get_compus()
    return JSONResponse(status_code=200, content=jsonable_encoder(result)) #lo convierte a JSON

@compu_router.get('/compus/{id}', tags=['compus'], response_model=Computadora)
def get_compu(id: int = Path(ge=1, le=2000)) -> Computadora: # significa que puede ser un id de 1 al 2000
    db = Session()
    result = ComputadoraService(db).get_compu(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "no encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@compu_router.get('/compus/', tags=['compus'], response_model=List[Computadora]) #query pone prametros o requierimientos extra para valaidar una entrada
def get_compus_by_marca(marca: str = Query(min_length=5, max_length=15)): #se pone el tipo de dato que usa el parametro,
    db = Session()
    result = ComputadoraService(db).get_compus_by_marca(marca)
    #el marca es por la cualidad del diccionario
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@compu_router.post('/compus', tags=['compus'], response_model=dict)
def create_compu(compu : Computadora) -> dict:
    db = Session()
    #usamos el modelo y le pasamos la info que vamos a registrar
    ComputadoraService(db).create_compu(compu)
    return JSONResponse(content={"message": "Se ha registrado la pelicula"})

@compu_router.delete('/compus/{id}', tags=['compus'])
def delete_compu(id: int):
    db = Session()
    ComputadoraService(db).delete_compu(id)

    return JSONResponse(content={"message": "Se ha Eliminado la pelicula"})


@compu_router.put('/compus/{id}', tags=['compus'], response_model=dict, status_code=200)
def updateCompus(id: int, compu: Computadora) -> dict:
    db = Session()
    result = ComputadoraService(db).get_compu(id)
    
    if not result:
        return JSONResponse(status_code=200, content={"message": "NO ENCONTRADO"})
    ComputadoraService(db).update_compu(id, compu)
    return JSONResponse(status_code=200, content={"message": "ACTUALIZADO"})