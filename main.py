from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.compus import compu_router
from routers.user import user_router
from routers.libro import libro_router
from routers.categorias import categoria_router
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
app.title = "fast api god Max yA NO TOMES CAFFEE"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)    
app.include_router(libro_router)
app.include_router(categoria_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Ajusta esto para restringir los orígenes permitidos
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



#Hola

@app.get('/', tags=['home']) #tags es para poder clasificar los endpoints y facilita su busqueda
def message():
    return HTMLResponse('<h1>Hola Mundo :)</h1>')






#realiza los endpoints para la venta de computadoras con una lista de 5 registros con 
#los siguientes valores
#id, marca, modelo, color, RAM y almacenamiento, realiza los ENDPOINTS ya hechos en clase
#en vex de categorias es get by marca, que se obtenga todos los detalles de las compus
# con esa marca, en caso de ser mas de una mostrar todas las de misma marca
