from pydantic import BaseModel, Field
from typing import Optional

class Libro(BaseModel):
    codigo: Optional[int] = None
    titulo: str = Field(min_length=2, max_length=55)
    autor: str = Field(min_length=2, max_length=55)
    ano: int = Field(ge=1, le=9000)
    categoria: str = Field(min_length=2, max_length=55)
    npag: int = Field(ge=1, le=9000)

    class Config:
        json_schema_extra = {
            "example:":{
                "codigo": 1,
                "titulo": "Harry Potter: El caliz de fuego",
                "autor": "Carlos Trejo",
                "ano": 2002,
                "categoria": "Accion",
                "npag": 400
            }
        }