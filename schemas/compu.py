from pydantic import BaseModel, Field
from typing import Optional

class Computadora(BaseModel):
    id: Optional[int] = None
    marca: str = Field(min_length=2, max_length=15)
    ram: int = Field(ge=1, le=2000)
    almacenamiento: int = Field(ge=1, le=9000)
    modelo: str = Field(min_length=4, max_length=55)
    color: str = Field(min_length=4, max_length=15)

    class Config:
        json_schema_extra = {
            "example:":{
                "id": 1,
                "marca": "Dell",
                "ram": 16,
                "almacenamiento": 512,
                "modelo": "3-89",
                "color": "Verde"
            }
        }