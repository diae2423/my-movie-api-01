from pydantic import BaseModel, Field
from typing import Optional

class Categoria(BaseModel):
    id: Optional[int] = None
    nombre: str = Field(min_length=2, max_length=55)
    

    class Config:
        json_schema_extra = {
            "example:":{
                "id": 1,
                "Nombre": "Drama"
            }
        }