from models.categoria import Categoria as CategoriaModel
from models.libro import Libro as LibroModel
from schemas.categoria import Categoria
class CategoriaService():
    def __init__(self, db) -> None:
            self.db = db

    def get_categotias(self):
        result = self.db.query(CategoriaModel).all()
        return result
    
    def get_categoria(self, codigo: int):
        result =  self.db.query(CategoriaModel).filter(CategoriaModel.id == codigo).first()
        return result
    
    def delete_categoria(self, codigo: int):
        result = self.db.query(CategoriaModel).filter(CategoriaModel.id == codigo).first()
        if not result:
            return {"message":"No existe"}
        self.db.delete(result)              
        self.db.commit()
        return {"message":"borrado"}
    
    def create_categoria(self, categoria : Categoria):
        new_categoria =  CategoriaModel(**categoria.model_dump())
        self.db.add(new_categoria)
        self.db.commit()
        return 
    
    def update_categoria(self, id: int, categoria : Categoria):
        
        result = self.db.query(CategoriaModel).filter(CategoriaModel.id == id).first()
        if not result:
            return {"message": "No existe"}
        nombre_anterior = result.nombre
        result.nombre = categoria.nombre

        librosConCategoria = self.db.query(LibroModel).filter(LibroModel.categoria == nombre_anterior).all()
        
        for libro in librosConCategoria:
            libro.categoria = categoria.nombre

        self.db.commit()
        return {"message": "ACTUALIZADO"}