from models.computadora import Computadora as ComputadoraModel
from schemas.compu import Computadora

class ComputadoraService():
    def __init__(self, db) -> None:
            self.db = db

    def get_compus(self):
        result = self.db.query(ComputadoraModel).all()
        return result
    
    def get_compu(self, id: int):
        result =  self.db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
        return result
    
    def get_compus_by_marca(self, marca: str):
        result =  self.db.query(ComputadoraModel).filter(ComputadoraModel.marca == marca).all()
        return result
    
    def create_compu(self, computadora : Computadora):
        new_Compu =  ComputadoraModel(**computadora.model_dump())
        self.db.add(new_Compu)
        self.db.commit()
        return 
    
    def delete_compu(self, id: int):
         result = self.db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
         if not result:
              return {"message":"No existe"}
         self.db.delete(result)              
         self.db.commit()
         return {"message":"borrado"}
    
    def update_compu(self, id: int, computadora : Computadora):
         result = self.db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
         if not result:
              return {"message":"No existe"}
         result.marca = computadora.marca
         result.ram = computadora.ram             
         result.almacenamiento = computadora.almacenamiento             
         result.modelo = computadora.modelo     
         result.color = computadora.color             
         self.db.commit()
         return {"message":"ACTUALIZADO"}

