from models.libro import Libro as LibroModel
from schemas.libro import Libro

class LibroService():
    def __init__(self, db) -> None:
            self.db = db

    def get_libros(self):
        result = self.db.query(LibroModel).all()
        return result
    
    def get_libro(self, codigo: int):
        result =  self.db.query(LibroModel).filter(LibroModel.codigo == codigo).first()
        return result
    
    def get_libros_by_category(self, category: str):
        result =  self.db.query(LibroModel).filter(LibroModel.categoria == category).all()
        return result
    
    def create_libro(self, libro : Libro):
        new_Libro =  LibroModel(**libro.model_dump())
        self.db.add(new_Libro)
        self.db.commit()
        return 
    
    def delete_libro(self, codigo: int):
         result = self.db.query(LibroModel).filter(LibroModel.codigo == codigo).first()
         if not result:
              return {"message":"No existe"}
         self.db.delete(result)              
         self.db.commit()
         return {"message":"borrado"}
    
    def update_libro(self, codigo: int, libro : Libro):
         result = self.db.query(LibroModel).filter(LibroModel.codigo == codigo).first()
         if not result:
              return {"message":"No existe"}
         result.titulo = libro.titulo
         result.autor = libro.autor             
         result.ano = libro.ano             
         result.categoria = libro.categoria     
         result.npag = libro.npag             
         self.db.commit()
         return {"message":"ACTUALIZADO"}

